from django.test import Client, TestCase
from .views import index
from home.models import Status
from django.urls import resolve
from django.apps import apps
from .forms import Status_Form
from .apps import *

# Create your tests here.

class trialTest(TestCase):

    def test_url_exist(self):
        response= Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_url_template(self):
        response= Client().get('/')
        self.assertTemplateUsed(response,'index.html')

    def test_url_func(self):
        found = resolve('/')
        self.assertEqual(found.func.__name__, index.__name__)

    def test_create_new_status(self):
        new= Status.objects.create(isi = 'nice')
        self.assertTrue(isinstance(new,Status))
        self.assertTrue(new.__str__(),new.isi)
        count_status = Status.objects.all().count()
        self.assertEqual(count_status, 1)

    def test_display_status(self):
        status ='fine'
        data = {'statu': status}
        post_data = Client().post('/', data)
        self.assertEqual(post_data.status_code,200)

    def test_application(self):
        self.assertEqual(HomeConfig.name,'home') 
        

    def test_hello(self):
        response = Client().get('/')
        response_content= response.content.decode('utf-8')
        self.assertIn("Hello",response_content)

    def test_form_validation(self):
        response = self.client.post('', {'isi':'fine'},follow=True)
        response_content= response.content.decode('utf-8')
        self.assertIn('isi',response_content)

    def test_create_m(self, isi='fine'):
        return Status.objects.create(isi=isi)

    def test_more_m(self):
        m= self.test_create_m()
        self.assertEqual(str(m),m.isi)


    def test_model_creation(self):
        m= self.test_create_m()
        self.assertTrue(isinstance(m,Status))
        self.assertEqual(m.__str__(),m.isi)       

    def test_invalid_post(self):
        form = Status_Form({'isi':''})
        self.assertFalse(form.is_valid())

    def test_form_new_isi(self):
        form= Status_Form({'isi':'fine'})
        self.assertTrue(form.is_valid())
                             