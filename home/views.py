from django.shortcuts import render
from .forms import Status_Form
from .models import Status
from django.http import HttpResponseRedirect, request

# Create your views here.

def index(request):
    
    statu = Status.objects.all()
    form= Status_Form()
    if (request.method=='POST'):
        form = Status_Form(request.POST)
        if(form.is_valid()):
            form.save()
        else:
            form= Status_Form()
    return render(request, 'index.html',{'form': form, 'status':statu})